Option Explicit

Const COUNTER_ACCOUNT_PRICE = "1.00"
Const FINAL_COLUMN_COUNT = 9

Const COL_TRANSACTION_ID = 0
Const COL_DATE = 1
Const COL_ACCOUNT = 2
Const COL_INVESTMENT = 3
Const COL_TRANSACTION_TYPE = 4
Const COL_DOLLAR = 5
Const COL_UNITS = 6
Const COL_DEPOSIT = 7
Const COL_PRICE = 8

Private Sub GetCounterAccountForTransactionType(transactionType as String) as String
	If transactionType = "RECORDKEEPING FEE" Then
		GetCounterAccountForTransactionType = "Expense"
	Else
		GetCounterAccountForTransactionType = "Placeholder"
	End If
End Sub

Private Sub GetDollarsPerUnit(sheet As Object, rowId As long) As Double
	Dim dollars As Currency
	Dim units As Double
	
	dollars = sheet.getCellByPosition(COL_DOLLAR, rowId).getValue()
	units = sheet.getCellByPosition(COL_UNITS, rowId).getValue()
	
	If units = 0 Then
		GetDollarsPerUnit = 0
	Else
		GetDollarsPerUnit = dollars / units
	End If
End Sub

Private Sub RemoveExtraInformationFromDate(dateString As String) As String
	RemoveExtraInformationFromDate = Split(dateString, " ", 2)(0)
End Sub

Private Sub ProcessRow(sheet As Object, rowId As long) As integer
	Dim cel As Object
	Dim dateFieldIsEmpty As boolean
	Dim doubledRowId As long
	Dim colId As long
	Dim transactionType As String
	
	cel = sheet.getCellByPosition(COL_DATE, rowId)

	If Len(cel.getString()) > 0 Then
		doubledRowId = rowId + 1
		
		sheet.getCellByPosition(COL_TRANSACTION_ID, rowId).setValue(rowId)
		sheet.getCellbyPosition(COL_DATE, rowID).setString(RemoveExtraInformationFromDate(sheet.getCellbyPosition(COL_DATE, rowID).getString()))
		
		sheet.Rows.insertByIndex(doubledRowId, 1)
		
		For colId=0 to FINAL_COLUMN_COUNT
			sheet.getCellByPosition(colId, doubledRowId).setString(sheet.getCellByPosition(colId, rowId).getString())
		Next
		
		transactionType = sheet.getCellByPosition(COL_TRANSACTION_TYPE, rowId).getString()
		sheet.getCellByPosition(COL_ACCOUNT, rowId).setString(GetCounterAccountForTransactionType(transactionType))
		sheet.getCellByPosition(COL_ACCOUNT, doubledRowId).setString(sheet.getCellByPosition(COL_INVESTMENT, doubledRowId).getString())
		
		sheet.getCellByPosition(COL_DEPOSIT, rowId).setString(sheet.getCellByPosition(COL_DOLLAR, rowId).getString())
		sheet.getCellByPosition(COL_DEPOSIT, doubledRowId).setString(sheet.getCellByPosition(COL_UNITS, doubledRowId).getString())
		
		sheet.getCellByPosition(COL_PRICE, rowId).setString(COUNTER_ACCOUNT_PRICE)
		sheet.getCellByPosition(COL_PRICE, doubledRowId).setString(GetDollarsPerUnit(sheet, rowId))
		
		ProcessRow = doubledRowId+1
	Else
		ProcessRow = -1
	End If
End Sub

sub PrepareSpreadsheet
	Dim sheet As Object
	Dim rowId As long

	sheet = ThisComponent.CurrentController.ActiveSheet
	
	sheet.Columns.insertByIndex(0, 1)
	sheet.Columns.insertByIndex(2, 1)
	
	sheet.getCellByPosition(COL_TRANSACTION_ID, 0).setString("Transaction ID")
	sheet.getCellByPosition(COL_ACCOUNT, 0).setString("Account")
	sheet.getCellByPosition(COL_DEPOSIT, 0).setString("Deposit")
	sheet.getCellByPosition(COL_PRICE, 0).setString("Price")

	rowId = 1
	Do While rowId > 0
		rowId = ProcessRow(sheet, rowId)
	Loop
End Sub
