# Fidelity Formatter

## Introduction
fidelity-formatter is a LibreOffice Basic macro to make it easier to import transaction history from Fidelity's 401k website in GnuCash.

## Directions
1. Copy and paste from the web-page table into a blank Calc sheet.
2. Copy the table from the sheet and paste as plain text.
3. Run the macro.
4. Save the sheet as a CSV file.
5. Import the transactions in GnuCash.
	* File Format: Multi-split as true.
	* Miscellaneous: Leading Lines to Skip as 1.
	* Use "Investment" column as "Description", "Transaction Type" as "Notes", and "Deposit" as "Withdrawal".
	* Do not use "Amount" or "# Shares or Units" columns.

## Notes/Bugs
Importing recordkeeping fees requires manual correction.
